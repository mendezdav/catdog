/***CAMBIOS A LA BASE************************/

/*TABLA MEDICINA*/
ALTER TABLE `catdog`.`medicinas` CHANGE COLUMN `cantidad` `cantidad` FLOAT(10,2) NULL DEFAULT NULL  ;
ALTER TABLE `catdog`.`fichapaciente` CHANGE COLUMN `Peso` `Peso` DECIMAL(10,2) NULL DEFAULT NULL  ;

ALTER TABLE `catdog`.`medicinaspaciente` CHANGE COLUMN `estado` `estado` VARCHAR(20) NOT NULL  ;
ALTER TABLE `catdog`.`medicinaspaciente` ADD COLUMN `fecha` TIMESTAMP NOT NULL  AFTER `estado` ;

ALTER TABLE `catdog`.`donaciones` CHANGE COLUMN `cantidadDonacion` `cantidadDonacion` DECIMAL(10,2) NULL DEFAULT NULL  ;


DELETE FROM `catdog`.`padrinospeludos`;
DELETE FROM `catdog`.`padrinos`;
INSERT INTO `catdog`.`padrinos` (`nombrePadrino`) VALUES ('Ninguno');

ALTER TABLE `catdog`.`donaciones` DROP FOREIGN KEY `fk_tipoInsumos_insumos` ;
ALTER TABLE `catdog`.`donaciones` DROP COLUMN `tipoInsumos`, DROP INDEX `fk_tipoInsumos_insumos` ;

ALTER TABLE `catdog`.`donaciones` ADD COLUMN `idinsumos` INT NOT NULL  AFTER `idDonacion` ;
ALTER TABLE `catdog`.`insumos` ADD CONSTRAINT `fk_tipo_insumos_id`
FOREIGN KEY (`tipoInsumo` )
REFERENCES `catdog`.`tipoinsumos` (`idtipoInsumos` )
ON DELETE NO ACTION
ON UPDATE CASCADE, ADD INDEX `fk_tipo_insumos_id` (`tipoInsumo` ASC) ;

ALTER TABLE `catdog`.`donaciones` 
  ADD CONSTRAINT `fk_id_insumos_donacion`
  FOREIGN KEY (`idinsumos` )
  REFERENCES `catdog`.`insumos` (`idinsumos` )
  ON DELETE NO ACTION
  ON UPDATE CASCADE
, ADD INDEX `fk_id_insumos_donacion` (`idinsumos` ASC) ;

ALTER TABLE `catdog`.`insumos` ADD COLUMN `nombre_insumo` VARCHAR(70) NULL  AFTER `idinsumos` 
, ADD UNIQUE INDEX `nombre_insumo_UNIQUE` (`nombre_insumo` ASC) ;

ALTER TABLE `catdog`.`tipoinsumos` ADD UNIQUE INDEX `tipoInsumo_UNIQUE` (`tipoInsumo` ASC) ;

ALTER TABLE `catdog`.`insumos` CHANGE COLUMN `nombre_insumo` `nombre_insumo` VARCHAR(70) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL  ;

INSERT INTO `catdog`.`tipousuario` (`tipoUsuario`, `descripcion`) VALUES ('Administrador', 'Administra el sitio');

INSERT INTO `catdog`.`tipousuario` (`tipoUsuario`, `descripcion`) VALUES ('Contador', 'Realiza los controles de inventario');

UPDATE `catdog`.`tipoinsumos` SET `tipoInsumo`='Comida' WHERE `idtipoInsumos`='1';

UPDATE `catdog`.`tipoinsumos` SET `tipoInsumo`='Material de limpieza' WHERE `idtipoInsumos`='2';

UPDATE `catdog`.`tipoinsumos` SET `tipoInsumo`='Papeler&iacute;a' WHERE `idtipoInsumos`='3';

ALTER TABLE `catdog`.`donaciones` CHANGE COLUMN `fechaDonacion` `fechaDonacion` DATE NOT NULL  ;

ALTER TABLE `catdog`.`donaciones` CHANGE COLUMN `donador` `donador` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT 'An&oacute;nimo'  ;

/******AGREGANDO TRIGGER************************/

 DELIMITER $$
 CREATE  TRIGGER actualizar_stock_insert
 AFTER INSERT  ON donaciones
 FOR EACH  ROW
    BEGIN
        DECLARE monto_final DECIMAL(10,2);
        DECLARE monto_actual DECIMAL(10,2);
        
        SET monto_actual = (select  cantidadTotal FROM insumos WHERE idinsumos = new.idinsumos);
        SET monto_final = monto_actual + new.cantidadDonacion;
        
        UPDATE insumos SET cantidadTotal = monto_final WHERE idinsumos = new.idinsumos;
   END $$

DELIMITER $$
CREATE  TRIGGER actualizar_stock_update
 AFTER UPDATE  ON donaciones
 FOR EACH  ROW
    BEGIN
        DECLARE monto_final DECIMAL(10,2);
        DECLARE monto_actual DECIMAL(10,2); 
        DECLARE idd INT;
        DECLARE m DECIMAL(10,2);
        /***CALCULANDO idinsumos*****/
        SET idd = new.idinsumos;
        SET monto_actual = (select  cantidadTotal FROM insumos WHERE idinsumos = new.idinsumos);
        SET monto_actual = monto_actual -  old.cantidadDonacion;
        SET monto_final = monto_actual + new.cantidadDonacion;
        UPDATE insumos SET cantidadTotal = monto_final WHERE idinsumos = new.idinsumos;
        if idd != old.idinsumos THEN
            SET monto_actual  =  (select  cantidadTotal FROM insumos WHERE idinsumos = old.idinsumos);
           SET m = monto_actual - new.cantidadDonacion;
           UPDATE insumos SET cantidadTotal = m WHERE idinsumos = old.idinsumos;
        end if;
   END $$

DELIMITER $$
CREATE
DEFINER=`root`@`localhost`
TRIGGER encriptar_3
before INSERT ON usuarios
FOR EACH ROW
begin
set NEW.password = AES_ENCRYPT(NEW.password,'catdog');
end;$$


/***********************ULTIMAS MODIFICACIONES***************************************/

ALTER TABLE `catdog`.`padrinos` ADD COLUMN `tipoPadrino` INT(11) NOT NULL  AFTER `email` , ADD COLUMN `cuota` DECIMAL(10,2) NOT NULL  AFTER `tipoPadrino` ;
ALTER TABLE `catdog`.`padrinos` CHANGE COLUMN `tipoPadrino` `tipo_padrino` INT(11) NOT NULL  ;
ALTER TABLE `catdog`.`padrinos` CHANGE COLUMN `tipo_padrino` `tipo_padrino` VARCHAR(30) NOT NULL  ;

INSERT INTO `catdog`.`razas` (`nombreRaza`, `decripcion`) VALUES ('Mezcla', 'Peludos mixtos');

