SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `catdog` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `catdog` ;

-- -----------------------------------------------------
-- Table `catdog`.`medidas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catdog`.`medidas` ;

CREATE  TABLE IF NOT EXISTS `catdog`.`medidas` (
  `idmedidas` INT(11) NOT NULL AUTO_INCREMENT ,
  `nombreMedida` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  PRIMARY KEY (`idmedidas`) )
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = '\n';


-- -----------------------------------------------------
-- Table `catdog`.`tipoinsumos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catdog`.`tipoinsumos` ;

CREATE  TABLE IF NOT EXISTS `catdog`.`tipoinsumos` (
  `idtipoInsumos` INT(11) NOT NULL AUTO_INCREMENT ,
  `tipoInsumo` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `idMedida` INT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`idtipoInsumos`) ,
  UNIQUE INDEX `tipoInsumo_UNIQUE` (`tipoInsumo` ASC) ,
  INDEX `fk_medida_tipoInsumo` (`idMedida` ASC) ,
  CONSTRAINT `fk_medida_tipoInsumo`
    FOREIGN KEY (`idMedida` )
    REFERENCES `catdog`.`medidas` (`idmedidas` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `catdog`.`insumos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catdog`.`insumos` ;

CREATE  TABLE IF NOT EXISTS `catdog`.`insumos` (
  `idinsumos` INT(11) NOT NULL AUTO_INCREMENT ,
  `nombre_insumo` VARCHAR(70) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `tipoInsumo` INT(11) NULL DEFAULT NULL ,
  `cantidadTotal` FLOAT(6,2) NOT NULL ,
  PRIMARY KEY (`idinsumos`) ,
  UNIQUE INDEX `nombre_insumo_UNIQUE` (`nombre_insumo` ASC) ,
  INDEX `fk_insumos_tipoInsumo` (`tipoInsumo` ASC) ,
  INDEX `fk_tipo_insumos_id` (`tipoInsumo` ASC) ,
  CONSTRAINT `fk_tipo_insumos_id`
    FOREIGN KEY (`tipoInsumo` )
    REFERENCES `catdog`.`tipoinsumos` (`idtipoInsumos` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `catdog`.`tipodonacion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catdog`.`tipodonacion` ;

CREATE  TABLE IF NOT EXISTS `catdog`.`tipodonacion` (
  `idtipoDonacion` INT(11) NOT NULL AUTO_INCREMENT ,
  `tipoDonacion` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `descripcion` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  PRIMARY KEY (`idtipoDonacion`) )
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `catdog`.`donaciones`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catdog`.`donaciones` ;

CREATE  TABLE IF NOT EXISTS `catdog`.`donaciones` (
  `idDonacion` INT(11) NOT NULL AUTO_INCREMENT ,
  `idinsumos` INT(11) NOT NULL ,
  `tipoDonacion` INT(11) NOT NULL ,
  `cantidadDonacion` DECIMAL(10,2) NULL DEFAULT NULL ,
  `fechaDonacion` DATE NOT NULL ,
  `donador` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT 'An&oacute;nimo' ,
  PRIMARY KEY (`idDonacion`) ,
  INDEX `fk_tipoDonacion_insumos` (`tipoDonacion` ASC) ,
  INDEX `fk_id_insumos_donacion` (`idinsumos` ASC) ,
  CONSTRAINT `fk_id_insumos_donacion`
    FOREIGN KEY (`idinsumos` )
    REFERENCES `catdog`.`insumos` (`idinsumos` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tipoDonacion_insumos`
    FOREIGN KEY (`tipoDonacion` )
    REFERENCES `catdog`.`tipodonacion` (`idtipoDonacion` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `catdog`.`especie`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catdog`.`especie` ;

CREATE  TABLE IF NOT EXISTS `catdog`.`especie` (
  `idespecie` INT(11) NOT NULL AUTO_INCREMENT ,
  `nombreEspecie` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  PRIMARY KEY (`idespecie`) )
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `catdog`.`razas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catdog`.`razas` ;

CREATE  TABLE IF NOT EXISTS `catdog`.`razas` (
  `idrazas` INT(11) NOT NULL AUTO_INCREMENT ,
  `nombreRaza` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `decripcion` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  PRIMARY KEY (`idrazas`) )
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `catdog`.`fichapaciente`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catdog`.`fichapaciente` ;

CREATE  TABLE IF NOT EXISTS `catdog`.`fichapaciente` (
  `idfichaPaciente` INT(11) NOT NULL AUTO_INCREMENT ,
  `nombrePeludo` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `Genero` CHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `edadEstimada` INT(11) NOT NULL ,
  `medidaEdad` INT(11) NOT NULL ,
  `fechaIngreso` DATE NOT NULL ,
  `colorPelo` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `colorOjos` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `Peso` DECIMAL(10,2) NULL DEFAULT NULL ,
  `raza` INT(11) NULL DEFAULT NULL ,
  `especie` INT(11) NOT NULL ,
  `fechaSalida` DATE NULL DEFAULT NULL ,
  `motivoEntrada` VARCHAR(120) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `motivoSalida` VARCHAR(120) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `foto` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `castrado` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `desparasitado` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `enTratamiento` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `procesoAdopcion` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  PRIMARY KEY (`idfichaPaciente`) ,
  INDEX `fk_raza_animales` (`raza` ASC) ,
  INDEX `fk_especie_animales` (`especie` ASC) ,
  INDEX `fk_medida_edades` (`medidaEdad` ASC) ,
  CONSTRAINT `fk_especie_animales`
    FOREIGN KEY (`especie` )
    REFERENCES `catdog`.`especie` (`idespecie` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_medida_edades`
    FOREIGN KEY (`medidaEdad` )
    REFERENCES `catdog`.`medidas` (`idmedidas` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_raza_animales`
    FOREIGN KEY (`raza` )
    REFERENCES `catdog`.`razas` (`idrazas` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `catdog`.`historiamedica`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catdog`.`historiamedica` ;

CREATE  TABLE IF NOT EXISTS `catdog`.`historiamedica` (
  `idhistoriaMedica` INT(11) NOT NULL AUTO_INCREMENT ,
  `idPaciente` INT(11) NOT NULL ,
  `diagnosticoClinico` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `fechaDiagnostico` DATE NOT NULL ,
  PRIMARY KEY (`idhistoriaMedica`) ,
  INDEX `fk_historia_paciente` (`idPaciente` ASC) ,
  CONSTRAINT `fk_historia_paciente`
    FOREIGN KEY (`idPaciente` )
    REFERENCES `catdog`.`fichapaciente` (`idfichaPaciente` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `catdog`.`medicinas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catdog`.`medicinas` ;

CREATE  TABLE IF NOT EXISTS `catdog`.`medicinas` (
  `idMedicinas` INT(11) NOT NULL AUTO_INCREMENT ,
  `nombreMedicina` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `cantidad` FLOAT(10,2) NULL DEFAULT NULL ,
  `idMedida` INT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`idMedicinas`) ,
  INDEX `fk_medicinas_medidas` (`idMedida` ASC) ,
  CONSTRAINT `fk_medicinas_medidas`
    FOREIGN KEY (`idMedida` )
    REFERENCES `catdog`.`medidas` (`idmedidas` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `catdog`.`medicinaspaciente`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catdog`.`medicinaspaciente` ;

CREATE  TABLE IF NOT EXISTS `catdog`.`medicinaspaciente` (
  `idmedicinasPaciente` INT(11) NOT NULL AUTO_INCREMENT ,
  `idPaciente` INT(11) NOT NULL ,
  `idMedicinas` INT(11) NOT NULL ,
  `estado` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `fecha` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP ,
  PRIMARY KEY (`idmedicinasPaciente`) ,
  INDEX `fk_medicinas_paciente` (`idMedicinas` ASC) ,
  INDEX `fk_pacientes_medicinas` (`idPaciente` ASC) ,
  INDEX `fk_medicinas_paciente_estado` (`estado` ASC) ,
  CONSTRAINT `fk_medicinas_paciente`
    FOREIGN KEY (`idMedicinas` )
    REFERENCES `catdog`.`medicinas` (`idMedicinas` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pacientes_medicinas`
    FOREIGN KEY (`idPaciente` )
    REFERENCES `catdog`.`fichapaciente` (`idfichaPaciente` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `catdog`.`padrinos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catdog`.`padrinos` ;

CREATE  TABLE IF NOT EXISTS `catdog`.`padrinos` (
  `idpadrinos` INT(11) NOT NULL AUTO_INCREMENT ,
  `nombrePadrino` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `telefonoContacto` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `email` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  PRIMARY KEY (`idpadrinos`) )
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `catdog`.`padrinospeludos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catdog`.`padrinospeludos` ;

CREATE  TABLE IF NOT EXISTS `catdog`.`padrinospeludos` (
  `idpadrinosPeludos` INT(11) NOT NULL AUTO_INCREMENT ,
  `idPadrinos` INT(11) NOT NULL ,
  `idFichaPaciente` INT(11) NOT NULL ,
  PRIMARY KEY (`idpadrinosPeludos`) ,
  INDEX `fk_padrino_padrinoPeludo` (`idPadrinos` ASC) ,
  INDEX `fk_ficha_padrinoPeludo` (`idFichaPaciente` ASC) ,
  CONSTRAINT `fk_ficha_padrinoPeludo`
    FOREIGN KEY (`idFichaPaciente` )
    REFERENCES `catdog`.`fichapaciente` (`idfichaPaciente` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_padrino_padrinoPeludo`
    FOREIGN KEY (`idPadrinos` )
    REFERENCES `catdog`.`padrinos` (`idpadrinos` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `catdog`.`tipousuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catdog`.`tipousuario` ;

CREATE  TABLE IF NOT EXISTS `catdog`.`tipousuario` (
  `idtipoUsuario` INT(11) NOT NULL AUTO_INCREMENT ,
  `tipoUsuario` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `descripcion` VARCHAR(120) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  PRIMARY KEY (`idtipoUsuario`) )
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `catdog`.`usuarios`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catdog`.`usuarios` ;

CREATE  TABLE IF NOT EXISTS `catdog`.`usuarios` (
  `idusuarios` INT(11) NOT NULL AUTO_INCREMENT ,
  `nickname` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `password` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `nombre` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `apellidos` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `tipoUsuario` INT(11) NOT NULL ,
  `foto` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  PRIMARY KEY (`idusuarios`) ,
  INDEX `fk_tipo_usuario` (`tipoUsuario` ASC) ,
  CONSTRAINT `fk_tipo_usuario`
    FOREIGN KEY (`tipoUsuario` )
    REFERENCES `catdog`.`tipousuario` (`idtipoUsuario` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `catdog`.`vacunas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catdog`.`vacunas` ;

CREATE  TABLE IF NOT EXISTS `catdog`.`vacunas` (
  `idvacunas` INT(11) NOT NULL AUTO_INCREMENT ,
  `nombreVacuna` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `frecuencia` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  PRIMARY KEY (`idvacunas`) )
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `catdog`.`vacunaspaciente`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catdog`.`vacunaspaciente` ;

CREATE  TABLE IF NOT EXISTS `catdog`.`vacunaspaciente` (
  `idvacunasPaciente` INT(11) NOT NULL AUTO_INCREMENT ,
  `idPaciente` INT(11) NOT NULL ,
  `idVacunas` INT(11) NOT NULL ,
  `estado` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  PRIMARY KEY (`idvacunasPaciente`) ,
  INDEX `fk_paciente_vacunas` (`idPaciente` ASC) ,
  INDEX `fk_vacunas_paciente` (`idVacunas` ASC) ,
  CONSTRAINT `fk_paciente_vacunas`
    FOREIGN KEY (`idPaciente` )
    REFERENCES `catdog`.`fichapaciente` (`idfichaPaciente` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_vacunas_paciente`
    FOREIGN KEY (`idVacunas` )
    REFERENCES `catdog`.`vacunas` (`idvacunas` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

USE `catdog`;

DELIMITER $$

USE `catdog`$$
DROP TRIGGER IF EXISTS `catdog`.`actualizar_stock_insert` $$
USE `catdog`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `catdog`.`actualizar_stock_insert`
AFTER INSERT ON `catdog`.`donaciones`
FOR EACH ROW
BEGIN
        DECLARE monto_final DECIMAL(10,2);
        DECLARE monto_actual DECIMAL(10,2);
        
        SET monto_actual = (select  cantidadTotal FROM insumos WHERE idinsumos = new.idinsumos);
        SET monto_final = monto_actual + new.cantidadDonacion;
        
        UPDATE insumos SET cantidadTotal = monto_final WHERE idinsumos = new.idinsumos;
   END$$


USE `catdog`$$
DROP TRIGGER IF EXISTS `catdog`.`actualizar_stock_update` $$
USE `catdog`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `catdog`.`actualizar_stock_update`
AFTER UPDATE ON `catdog`.`donaciones`
FOR EACH ROW
BEGIN
        DECLARE monto_final DECIMAL(10,2);
        DECLARE monto_actual DECIMAL(10,2); 
        DECLARE idd INT;
        DECLARE m DECIMAL(10,2);
        /***CALCULANDO idinsumos*****/
        SET idd = new.idinsumos;
        if idd != old.idinsumos THEN
            SET monto_actual  =  (select  cantidadTotal FROM insumos WHERE idinsumos = old.idinsumos);
            SET m = monto_actual - new.cantidadDonacion;
            SET monto_actual = monto_actual -  old.cantidadDonacion;
            SET monto_final = monto_actual + new.cantidadDonacion;
            UPDATE insumos SET cantidadTotal = m WHERE idinsumos = old.idinsumos;
            UPDATE insumos SET cantidadTotal = monto_final WHERE idinsumos = new.idinsumos;
        else
             SET monto_actual = (select  cantidadTotal FROM insumos WHERE idinsumos = new.idinsumos);
            SET monto_actual = monto_actual -  old.cantidadDonacion;
            SET monto_final = monto_actual + new.cantidadDonacion;
            UPDATE insumos SET cantidadTotal = monto_final WHERE idinsumos = new.idinsumos;
        end if;
   END$$


CREATE DEFINER=`root`@`localhost`
TRIGGER encriptar_3
BEFORE INSERT ON usuarios
FOR EACH ROW
begin
set NEW.password = AES_ENCRYPT(NEW.password,'catdog');
END$$

DELIMITER ;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

/*******************************************
	alter table */
alter table usuarios modify password varbinary(2000);

/**********************************************
*           INSERTS DE LA BASE
*
***********************************************/

INSERT INTO `catdog`.`padrinos` (`nombrePadrino`) VALUES ('Ninguno');
INSERT INTO `catdog`.`tipousuario` (`tipoUsuario`, `descripcion`) VALUES ('Administrador', 'Administra el sitio');
INSERT INTO `catdog`.`tipousuario` (`tipoUsuario`, `descripcion`) VALUES ('Contador', 'Realiza los controles de inventario');

INSERT INTO `catdog`.`tipoinsumos` (`tipoInsumo`) VALUES ('Comida');
INSERT INTO `catdog`.`tipoinsumos` (`tipoInsumo`) VALUES ('Material de limpIeza');
INSERT INTO `catdog`.`tipoinsumos` (`tipoInsumo`) VALUES ('Papeler&iacute;a');
