-- MySQL dump 10.11
--
-- Host: localhost    Database: catdog
-- ------------------------------------------------------
-- Server version	5.0.45-community-nt

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `donaciones`
--
use catdog;
DROP TABLE IF EXISTS `donaciones`;
CREATE TABLE `donaciones` (
  `idDonacion` int(11) NOT NULL auto_increment,
  `idinsumos` int(11) NOT NULL,
  `tipoDonacion` int(11) NOT NULL,
  `cantidadDonacion` decimal(10,2) default NULL,
  `fechaDonacion` date NOT NULL,
  `donador` varchar(45) collate utf8_unicode_ci default 'An&oacute;nimo',
  PRIMARY KEY  (`idDonacion`),
  KEY `fk_tipoDonacion_insumos` (`tipoDonacion`),
  KEY `fk_id_insumos_donacion` (`idinsumos`),
  CONSTRAINT `fk_id_insumos_donacion` FOREIGN KEY (`idinsumos`) REFERENCES `insumos` (`idinsumos`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_tipoDonacion_insumos` FOREIGN KEY (`tipoDonacion`) REFERENCES `tipodonacion` (`idtipoDonacion`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `donaciones`
--

LOCK TABLES `donaciones` WRITE;
/*!40000 ALTER TABLE `donaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `donaciones` ENABLE KEYS */;
UNLOCK TABLES;

DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `actualizar_stock_insert` AFTER INSERT ON `donaciones` FOR EACH ROW BEGIN
        DECLARE monto_final DECIMAL(10,2);
        DECLARE monto_actual DECIMAL(10,2);
        
        SET monto_actual = (select  cantidadTotal FROM insumos WHERE idinsumos = new.idinsumos);
        SET monto_final = monto_actual + new.cantidadDonacion;
        
        UPDATE insumos SET cantidadTotal = monto_final WHERE idinsumos = new.idinsumos;
   END */;;

/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `actualizar_stock_update` AFTER UPDATE ON `donaciones` FOR EACH ROW BEGIN
        DECLARE monto_final DECIMAL(10,2);
        DECLARE monto_actual DECIMAL(10,2); 
        DECLARE idd INT;
        DECLARE m DECIMAL(10,2);
        /***CALCULANDO idinsumos*****/
        SET idd = new.idinsumos;
        if idd != old.idinsumos THEN
            SET monto_actual  =  (select  cantidadTotal FROM insumos WHERE idinsumos = old.idinsumos);
            SET m = monto_actual - new.cantidadDonacion;
            SET monto_actual = monto_actual -  old.cantidadDonacion;
            SET monto_final = monto_actual + new.cantidadDonacion;
            UPDATE insumos SET cantidadTotal = m WHERE idinsumos = old.idinsumos;
            UPDATE insumos SET cantidadTotal = monto_final WHERE idinsumos = new.idinsumos;
        else
             SET monto_actual = (select  cantidadTotal FROM insumos WHERE idinsumos = new.idinsumos);
            SET monto_actual = monto_actual -  old.cantidadDonacion;
            SET monto_final = monto_actual + new.cantidadDonacion;
            UPDATE insumos SET cantidadTotal = monto_final WHERE idinsumos = new.idinsumos;
        end if;
   END */;;

DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;

--
-- Table structure for table `especie`
--

DROP TABLE IF EXISTS `especie`;
CREATE TABLE `especie` (
  `idespecie` int(11) NOT NULL auto_increment,
  `nombreEspecie` varchar(45) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`idespecie`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `especie`
--

LOCK TABLES `especie` WRITE;
/*!40000 ALTER TABLE `especie` DISABLE KEYS */;
INSERT INTO `especie` VALUES (1,'Perro'),(2,'Gato'),(3,'Mapache');
/*!40000 ALTER TABLE `especie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fichapaciente`
--

DROP TABLE IF EXISTS `fichapaciente`;
CREATE TABLE `fichapaciente` (
  `idfichaPaciente` int(11) NOT NULL auto_increment,
  `nombrePeludo` varchar(45) collate utf8_unicode_ci NOT NULL,
  `Genero` char(2) collate utf8_unicode_ci NOT NULL,
  `edadEstimada` int(11) NOT NULL,
  `medidaEdad` int(11) NOT NULL,
  `fechaIngreso` date NOT NULL,
  `colorPelo` varchar(45) collate utf8_unicode_ci default NULL,
  `colorOjos` varchar(45) collate utf8_unicode_ci default NULL,
  `Peso` decimal(10,2) default NULL,
  `raza` int(11) default NULL,
  `especie` int(11) NOT NULL,
  `fechaSalida` date default NULL,
  `motivoEntrada` varchar(120) collate utf8_unicode_ci NOT NULL,
  `motivoSalida` varchar(120) collate utf8_unicode_ci default NULL,
  `foto` text collate utf8_unicode_ci,
  `castrado` varchar(10) collate utf8_unicode_ci NOT NULL,
  `desparasitado` varchar(10) collate utf8_unicode_ci NOT NULL,
  `enTratamiento` varchar(10) collate utf8_unicode_ci NOT NULL,
  `procesoAdopcion` varchar(10) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`idfichaPaciente`),
  KEY `fk_raza_animales` (`raza`),
  KEY `fk_especie_animales` (`especie`),
  KEY `fk_medida_edades` (`medidaEdad`),
  CONSTRAINT `fk_especie_animales` FOREIGN KEY (`especie`) REFERENCES `especie` (`idespecie`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_medida_edades` FOREIGN KEY (`medidaEdad`) REFERENCES `medidas` (`idmedidas`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_raza_animales` FOREIGN KEY (`raza`) REFERENCES `razas` (`idrazas`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fichapaciente`
--

LOCK TABLES `fichapaciente` WRITE;
/*!40000 ALTER TABLE `fichapaciente` DISABLE KEYS */;
INSERT INTO `fichapaciente` VALUES (1,'pringuita 1','H',8,3,'2013-09-29','','','52.30',1,1,'0000-00-00','						\r\n					','','','Si','Si','Si','Si'),(2,'Firulais','M',4,3,'2013-09-28','Negro','Negro','30.00',8,1,'0000-00-00','					Entró por atropello en ciudad delgado					\r\n					\r\n					','										\r\n					\r\n					','','Sí','Sí','Sí','Sí'),(3,'Perlita','H',6,3,'2013-09-29','Negro','Negro','30.00',5,1,'0000-00-00','Entró por abandono																						\r\n					\r\n					\r\n					\r\n					','Salió por adopción																\r\n					\r\n					\r\n					','','Si','Si','Si','Si'),(5,'Lucky','M',6,3,'2013-10-28','','','30.00',1,1,'0000-00-00','						\r\n					','','','Si','Si','Si','Si'),(6,'Osito','M',5,3,'2013-10-28','Café','Café','100.00',4,1,'0000-00-00','						\r\n					','','','Si','Si','Si','Si'),(7,'Cleo','H',3,3,'2013-10-28','Negro','Negro','150.00',1,1,'0000-00-00','						\r\n					','','','Si','Si','Si','Si');
/*!40000 ALTER TABLE `fichapaciente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historiamedica`
--

DROP TABLE IF EXISTS `historiamedica`;
CREATE TABLE `historiamedica` (
  `idhistoriaMedica` int(11) NOT NULL auto_increment,
  `idPaciente` int(11) NOT NULL,
  `diagnosticoClinico` varchar(255) collate utf8_unicode_ci NOT NULL,
  `fechaDiagnostico` date NOT NULL,
  PRIMARY KEY  (`idhistoriaMedica`),
  KEY `fk_historia_paciente` (`idPaciente`),
  CONSTRAINT `fk_historia_paciente` FOREIGN KEY (`idPaciente`) REFERENCES `fichapaciente` (`idfichaPaciente`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `historiamedica`
--

LOCK TABLES `historiamedica` WRITE;
/*!40000 ALTER TABLE `historiamedica` DISABLE KEYS */;
INSERT INTO `historiamedica` VALUES (4,1,'Pringuita necesita lavado bucal','2013-10-22'),(6,1,'Pringuita necesita lavado bucal','2013-10-04'),(7,1,'Pringuita necesita lavado bucal','2013-10-04'),(8,1,'Pringuita necesita lavado bucal','2013-09-11');
/*!40000 ALTER TABLE `historiamedica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `insumos`
--

DROP TABLE IF EXISTS `insumos`;
CREATE TABLE `insumos` (
  `idinsumos` int(11) NOT NULL auto_increment,
  `nombre_insumo` varchar(70) collate utf8_unicode_ci NOT NULL,
  `tipoInsumo` int(11) default NULL,
  `cantidadTotal` float(6,2) NOT NULL,
  PRIMARY KEY  (`idinsumos`),
  UNIQUE KEY `nombre_insumo_UNIQUE` (`nombre_insumo`),
  KEY `fk_insumos_tipoInsumo` (`tipoInsumo`),
  KEY `fk_tipo_insumos_id` (`tipoInsumo`),
  CONSTRAINT `fk_tipo_insumos_id` FOREIGN KEY (`tipoInsumo`) REFERENCES `tipoinsumos` (`idtipoInsumos`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `insumos`
--

LOCK TABLES `insumos` WRITE;
/*!40000 ALTER TABLE `insumos` DISABLE KEYS */;
INSERT INTO `insumos` VALUES (1,'Pedigree adulto',1,200.00),(2,'Pedrigree cachorro',1,150.00),(5,'Detergente Xedex',1,150.00);
/*!40000 ALTER TABLE `insumos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medicinas`
--

DROP TABLE IF EXISTS `medicinas`;
CREATE TABLE `medicinas` (
  `idMedicinas` int(11) NOT NULL auto_increment,
  `nombreMedicina` varchar(100) collate utf8_unicode_ci NOT NULL,
  `cantidad` float(10,2) default NULL,
  `idMedida` int(11) default NULL,
  PRIMARY KEY  (`idMedicinas`),
  KEY `fk_medicinas_medidas` (`idMedida`),
  CONSTRAINT `fk_medicinas_medidas` FOREIGN KEY (`idMedida`) REFERENCES `medidas` (`idmedidas`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `medicinas`
--

LOCK TABLES `medicinas` WRITE;
/*!40000 ALTER TABLE `medicinas` DISABLE KEYS */;
INSERT INTO `medicinas` VALUES (2,'enalcot',9.99,5),(3,'acetaminofén',50.00,6),(4,'Lexitina',10.00,5);
/*!40000 ALTER TABLE `medicinas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medicinaspaciente`
--

DROP TABLE IF EXISTS `medicinaspaciente`;
CREATE TABLE `medicinaspaciente` (
  `idmedicinasPaciente` int(11) NOT NULL auto_increment,
  `idPaciente` int(11) NOT NULL,
  `idMedicinas` int(11) NOT NULL,
  `estado` varchar(20) collate utf8_unicode_ci NOT NULL,
  `fecha` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`idmedicinasPaciente`),
  KEY `fk_medicinas_paciente` (`idMedicinas`),
  KEY `fk_pacientes_medicinas` (`idPaciente`),
  KEY `fk_medicinas_paciente_estado` (`estado`),
  CONSTRAINT `fk_medicinas_paciente` FOREIGN KEY (`idMedicinas`) REFERENCES `medicinas` (`idMedicinas`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_pacientes_medicinas` FOREIGN KEY (`idPaciente`) REFERENCES `fichapaciente` (`idfichaPaciente`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `medicinaspaciente`
--

LOCK TABLES `medicinaspaciente` WRITE;
/*!40000 ALTER TABLE `medicinaspaciente` DISABLE KEYS */;
INSERT INTO `medicinaspaciente` VALUES (1,5,3,'Aplicada','0000-00-00 00:00:00'),(2,5,4,'Aplicada','0000-00-00 00:00:00'),(3,5,3,'Aplicada','0000-00-00 00:00:00'),(4,5,2,'Aplicada','0000-00-00 00:00:00'),(5,5,2,'Aplicada','2013-10-28 15:10:54'),(6,5,2,'Aplicada','2013-10-28 15:10:06');
/*!40000 ALTER TABLE `medicinaspaciente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medidas`
--

DROP TABLE IF EXISTS `medidas`;
CREATE TABLE `medidas` (
  `idmedidas` int(11) NOT NULL auto_increment,
  `nombreMedida` varchar(45) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`idmedidas`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='\n';

--
-- Dumping data for table `medidas`
--

LOCK TABLES `medidas` WRITE;
/*!40000 ALTER TABLE `medidas` DISABLE KEYS */;
INSERT INTO `medidas` VALUES (1,'Libras'),(2,'Dólares'),(3,'Años'),(4,'Meses'),(5,'Mililitros'),(6,'Cajas'),(7,'Ampolletas');
/*!40000 ALTER TABLE `medidas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `padrinos`
--

DROP TABLE IF EXISTS `padrinos`;
CREATE TABLE `padrinos` (
  `idpadrinos` int(11) NOT NULL auto_increment,
  `nombrePadrino` varchar(45) collate utf8_unicode_ci NOT NULL,
  `telefonoContacto` varchar(10) collate utf8_unicode_ci default NULL,
  `email` varchar(100) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`idpadrinos`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `padrinos`
--

LOCK TABLES `padrinos` WRITE;
/*!40000 ALTER TABLE `padrinos` DISABLE KEYS */;
INSERT INTO `padrinos` VALUES (1,'Ninguno',NULL,NULL);
/*!40000 ALTER TABLE `padrinos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `padrinospeludos`
--

DROP TABLE IF EXISTS `padrinospeludos`;
CREATE TABLE `padrinospeludos` (
  `idpadrinosPeludos` int(11) NOT NULL auto_increment,
  `idPadrinos` int(11) NOT NULL,
  `idFichaPaciente` int(11) NOT NULL,
  PRIMARY KEY  (`idpadrinosPeludos`),
  KEY `fk_padrino_padrinoPeludo` (`idPadrinos`),
  KEY `fk_ficha_padrinoPeludo` (`idFichaPaciente`),
  CONSTRAINT `fk_ficha_padrinoPeludo` FOREIGN KEY (`idFichaPaciente`) REFERENCES `fichapaciente` (`idfichaPaciente`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_padrino_padrinoPeludo` FOREIGN KEY (`idPadrinos`) REFERENCES `padrinos` (`idpadrinos`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `padrinospeludos`
--

LOCK TABLES `padrinospeludos` WRITE;
/*!40000 ALTER TABLE `padrinospeludos` DISABLE KEYS */;
/*!40000 ALTER TABLE `padrinospeludos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `razas`
--

DROP TABLE IF EXISTS `razas`;
CREATE TABLE `razas` (
  `idrazas` int(11) NOT NULL auto_increment,
  `nombreRaza` varchar(50) collate utf8_unicode_ci NOT NULL,
  `decripcion` varchar(50) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`idrazas`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `razas`
--

LOCK TABLES `razas` WRITE;
/*!40000 ALTER TABLE `razas` DISABLE KEYS */;
INSERT INTO `razas` VALUES (1,'Aguacatero','Peludos sin una raza en particular'),(2,'Pitbull',NULL),(3,'Chihuahua',NULL),(4,'Rottwailer',NULL),(5,'Doverman',NULL),(6,'Dálmata',NULL),(7,'Cocker Spaniel',NULL),(8,'Beagle',NULL),(9,'Siberiano',NULL),(10,'Golden retriever',NULL);
/*!40000 ALTER TABLE `razas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipodonacion`
--

DROP TABLE IF EXISTS `tipodonacion`;
CREATE TABLE `tipodonacion` (
  `idtipoDonacion` int(11) NOT NULL auto_increment,
  `tipoDonacion` varchar(30) collate utf8_unicode_ci NOT NULL,
  `descripcion` varchar(100) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`idtipoDonacion`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tipodonacion`
--

LOCK TABLES `tipodonacion` WRITE;
/*!40000 ALTER TABLE `tipodonacion` DISABLE KEYS */;
INSERT INTO `tipodonacion` VALUES (1,'Voluntaria','Donaciones por voluntarios del refugio y otros'),(2,'Apadrinamiento','Padrinos'),(4,'Material clínico','Materiales para las consultas veterinarias');
/*!40000 ALTER TABLE `tipodonacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoinsumos`
--

DROP TABLE IF EXISTS `tipoinsumos`;
CREATE TABLE `tipoinsumos` (
  `idtipoInsumos` int(11) NOT NULL auto_increment,
  `tipoInsumo` varchar(40) collate utf8_unicode_ci NOT NULL,
  `idMedida` int(11) default NULL,
  PRIMARY KEY  (`idtipoInsumos`),
  UNIQUE KEY `tipoInsumo_UNIQUE` (`tipoInsumo`),
  KEY `fk_medida_tipoInsumo` (`idMedida`),
  CONSTRAINT `fk_medida_tipoInsumo` FOREIGN KEY (`idMedida`) REFERENCES `medidas` (`idmedidas`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tipoinsumos`
--

LOCK TABLES `tipoinsumos` WRITE;
/*!40000 ALTER TABLE `tipoinsumos` DISABLE KEYS */;
INSERT INTO `tipoinsumos` VALUES (1,'Comida',1),(2,'Material de limpieza',6),(3,'Papeler&iacute;a',6);
/*!40000 ALTER TABLE `tipoinsumos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipousuario`
--

DROP TABLE IF EXISTS `tipousuario`;
CREATE TABLE `tipousuario` (
  `idtipoUsuario` int(11) NOT NULL auto_increment,
  `tipoUsuario` varchar(45) collate utf8_unicode_ci NOT NULL,
  `descripcion` varchar(120) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`idtipoUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tipousuario`
--

LOCK TABLES `tipousuario` WRITE;
/*!40000 ALTER TABLE `tipousuario` DISABLE KEYS */;
INSERT INTO `tipousuario` VALUES (1,'Administrador','Administra el sitio'),(2,'Contador','Realiza los controles de inventario');
/*!40000 ALTER TABLE `tipousuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `idusuarios` int(11) NOT NULL auto_increment,
  `nickname` varchar(45) collate utf8_unicode_ci NOT NULL,
  `password` varchar(255) collate utf8_unicode_ci NOT NULL,
  `nombre` varchar(45) collate utf8_unicode_ci NOT NULL,
  `apellidos` varchar(50) collate utf8_unicode_ci default NULL,
  `tipoUsuario` int(11) NOT NULL,
  `foto` text collate utf8_unicode_ci,
  PRIMARY KEY  (`idusuarios`),
  KEY `fk_tipo_usuario` (`tipoUsuario`),
  CONSTRAINT `fk_tipo_usuario` FOREIGN KEY (`tipoUsuario`) REFERENCES `tipousuario` (`idtipoUsuario`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'pochii','123456','Alejandra ','Meléndez',1,'');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `encriptar_3` BEFORE INSERT ON `usuarios` FOR EACH ROW begin
set NEW.password = AES_ENCRYPT(NEW.password,'catdog');
END */;;

DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;

--
-- Table structure for table `vacunas`
--

DROP TABLE IF EXISTS `vacunas`;
CREATE TABLE `vacunas` (
  `idvacunas` int(11) NOT NULL auto_increment,
  `nombreVacuna` varchar(45) collate utf8_unicode_ci NOT NULL,
  `frecuencia` varchar(45) collate utf8_unicode_ci default NULL,
  `descripcion` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`idvacunas`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vacunas`
--

LOCK TABLES `vacunas` WRITE;
/*!40000 ALTER TABLE `vacunas` DISABLE KEYS */;
INSERT INTO `vacunas` VALUES (1,'Penicilina','diaria','Para dolor de huesos'),(2,'Antirrábica','Anual','Para perros y gatos');
/*!40000 ALTER TABLE `vacunas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacunaspaciente`
--

DROP TABLE IF EXISTS `vacunaspaciente`;
CREATE TABLE `vacunaspaciente` (
  `idvacunasPaciente` int(11) NOT NULL auto_increment,
  `idPaciente` int(11) NOT NULL,
  `idVacunas` int(11) NOT NULL,
  `estado` varchar(20) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`idvacunasPaciente`),
  KEY `fk_paciente_vacunas` (`idPaciente`),
  KEY `fk_vacunas_paciente` (`idVacunas`),
  CONSTRAINT `fk_paciente_vacunas` FOREIGN KEY (`idPaciente`) REFERENCES `fichapaciente` (`idfichaPaciente`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_vacunas_paciente` FOREIGN KEY (`idVacunas`) REFERENCES `vacunas` (`idvacunas`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vacunaspaciente`
--

LOCK TABLES `vacunaspaciente` WRITE;
/*!40000 ALTER TABLE `vacunaspaciente` DISABLE KEYS */;
/*!40000 ALTER TABLE `vacunaspaciente` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-11-08 21:31:12

/*****TRIGGERS*************************/

DELIMITER $$

USE `catdog`$$
DROP TRIGGER IF EXISTS `catdog`.`actualizar_stock_insert` $$
USE `catdog`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `catdog`.`actualizar_stock_insert`
AFTER INSERT ON `catdog`.`donaciones`
FOR EACH ROW
BEGIN
        DECLARE monto_final DECIMAL(10,2);
        DECLARE monto_actual DECIMAL(10,2);
        
        SET monto_actual = (select  cantidadTotal FROM insumos WHERE idinsumos = new.idinsumos);
        SET monto_final = monto_actual + new.cantidadDonacion;
        
        UPDATE insumos SET cantidadTotal = monto_final WHERE idinsumos = new.idinsumos;
   END$$


USE `catdog`$$
DROP TRIGGER IF EXISTS `catdog`.`actualizar_stock_update` $$
USE `catdog`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `catdog`.`actualizar_stock_update`
AFTER UPDATE ON `catdog`.`donaciones`
FOR EACH ROW
BEGIN
        DECLARE monto_final DECIMAL(10,2);
        DECLARE monto_actual DECIMAL(10,2); 
        DECLARE idd INT;
        DECLARE m DECIMAL(10,2);
        /***CALCULANDO idinsumos*****/
        SET idd = new.idinsumos;
        if idd != old.idinsumos THEN
            SET monto_actual  =  (select  cantidadTotal FROM insumos WHERE idinsumos = old.idinsumos);
            SET m = monto_actual - new.cantidadDonacion;
            SET monto_actual = monto_actual -  old.cantidadDonacion;
            SET monto_final = monto_actual + new.cantidadDonacion;
            UPDATE insumos SET cantidadTotal = m WHERE idinsumos = old.idinsumos;
            UPDATE insumos SET cantidadTotal = monto_final WHERE idinsumos = new.idinsumos;
        else
             SET monto_actual = (select  cantidadTotal FROM insumos WHERE idinsumos = new.idinsumos);
            SET monto_actual = monto_actual -  old.cantidadDonacion;
            SET monto_final = monto_actual + new.cantidadDonacion;
            UPDATE insumos SET cantidadTotal = monto_final WHERE idinsumos = new.idinsumos;
        end if;
   END$$


CREATE DEFINER=`root`@`localhost`
TRIGGER encriptar_3
BEFORE INSERT ON usuarios
FOR EACH ROW
begin
set NEW.password = AES_ENCRYPT(NEW.password,'catdog');
END$$

DELIMITER ;


/*******************************************
  alter table */
alter table usuarios modify password varbinary(2000);
/**********************************************
*           INSERTS DE LA BASE
*
***********************************************/

INSERT INTO `catdog`.`padrinos` (`nombrePadrino`) VALUES ('Ninguno');
INSERT INTO `catdog`.`tipousuario` (`tipoUsuario`, `descripcion`) VALUES ('Administrador', 'Administra el sitio');
INSERT INTO `catdog`.`tipousuario` (`tipoUsuario`, `descripcion`) VALUES ('Contador', 'Realiza los controles de inventario');

INSERT INTO `catdog`.`tipoinsumos` (`tipoInsumo`) VALUES ('Comida');
INSERT INTO `catdog`.`tipoinsumos` (`tipoInsumo`) VALUES ('Material de limpIeza');
INSERT INTO `catdog`.`tipoinsumos` (`tipoInsumo`) VALUES ('Papeler&iacute;a');
