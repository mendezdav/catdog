<?php
	
	import('mdl.model.donaciones');
	import('mdl.view.donaciones');

	function validar(){
		if(!Session::singleton()->ValidateSession()){
			HttpHandler::redirect('/catdog/sistema/login');
		}else{
			if(Session::getLevel()!=1){
				HttpHandler::redirect('/catdog/sistema/inicio?ERR=NA');
			}
		}
	}

	validar();

	class donacionesController extends controller
	{

		public function panel(){
			$this->view->panel();
		}

		public function guardarDonaciones()
		{
			if (isset($_POST) && !empty($_POST)):			
				$this->model->get(0);
				$this->model->change_status($_POST);
				$this->model->save();
				HttpHandler::redirect('/catdog/donaciones/formularioListar?conf=Ok');
				else:
					echo "La funcion no fue llamada desde formulario";
				endif;
			
		}

		public function borrarDonacion()
		{
			$id     = isset($_GET['id'])?$_GET['id']:'0';
			$iModel = $this->model->get_sibling('insumos');

			$this->model->get($id);

			$insumo = $this->model->idinsumos;
			$monto	= $this->model->cantidadDonacion;

			$iModel->get($insumo);
			$cactual               = $iModel->cantidadTotal; 
			$iModel->cantidadTotal = $cactual - $monto;
			$iModel->save();

			$this->model->delete($id);
			HttpHandler::redirect('/catdog/donaciones/formularioListar?del=Ok');

		}

		public function GuardarModificarDonaciones()
		{
			if (isset($_POST) && !empty($_POST)):
			
				$data = $_POST;
				$id = empty($data['id'])?0:$data['id'];
				unset($data['id']);
				$this->model->get($id);
				$this->model->change_status($_POST);
				$this->model->save();
				HttpHandler::redirect('/catdog/donaciones/formularioListar?up=Ok');
				else:
					echo "La funcion no fue llamada desde formulario";
				endif;

		}



		public function modificarDonaciones()
		{
			
			$id = isset($_GET['id'])?$_GET['id']:'0';
			$cache = array();

			if ($this->model->exists($id))
			{

				$cache[0] = $this->model->listar_insumos();
				$cache[1] = $this->model->get_child("tipoDonacion")->get_list();
				$cache[2] = $this->model->editar($id);
				$cache[3] = $this->model->insumo_actual($id);
				$cache[4] = $this->model->donacion_actual($id);
				$this->view->generarFmodificar($cache);
			}
			else 
			{
				HttpHandler::redirect('/catdog/donaciones/formularioIngreso');
			}

						
		}

		public function formularioIngreso()
		{
			$cache = array();
			$cache[0] = $this->model->listar_insumos();
			$cache[1] = $this->model->get_child("tipoDonacion")->get_list();
			//$this->view->generarCombo($cache);
			
			$this->view->generarFingreso($cache);
		}

		public function formularioListar()
		{
			$cache = array();
			$cache[0] = $this->model->listar();
			$this->view->generarFlistar($cache);
			
			
		}
		
			
	}

?>