<?php

	import("mdl.model.razas");
	import("mdl.view.razas");

	function validar(){
		if(!Session::singleton()->ValidateSession()){
			HttpHandler::redirect('/catdog/sistema/login');
		}else{
			if(Session::getLevel()!=1){
				HttpHandler::redirect('/catdog/sistema/inicio?ERR=NA');
			}
		}
	}

	validar();

	class razasController extends controller
	{
		public function reporte(){
			$cache = array();
			$cache[0] = $this->model->get_list();
			$cache[1] = $this->model->consultar();	
			$this->view->imprimirReporte($cache);
		}
	}
?>