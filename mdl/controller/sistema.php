<?php

	import('mdl.view.sistema');

	function validar(){
		if(!Session::singleton()->ValidateSession()){
			HttpHandler::redirect('/catdog/sistema/login');
		}
	}

	class sistemaController extends controller{

		/* sobre escribimos el constructor para librar el controlador del model asociado */
		public function __construct($resource='',$argv=''){
			
			$this->view = Helper::get_view($this); // solo se carga la vista

		}

		/* funcion que provee acceso a los modulos */
		public function inicio(){
			validar();
			$this->view->index(); // cargar el index.html [ver: ../mdl/view/sistema.php]

		}

		public function signin(){
			if(!Session::ValidateSession())
				$this->view->signin();
			else
				HttpHandler::redirect('/catdog/sistema/inicio');
		}

		public function login(){
			$usuario = $_POST['usuario'];
			$clave = $_POST['clave'];
			$query = "SELECT * FROM usuarios WHERE nickname='{$usuario}' AND AES_DECRYPT(password,'catdog')='{$clave}';";
			data_model()->executeQuery($query);
			if(data_model()->getNumRows()>0){
				$dat = data_model()->getResult()->fetch_assoc();
				$tipo = $dat['tipoUsuario'];
				Session::singleton()->NewSession($usuario,$tipo);
				HttpHandler::redirect('/catdog/sistema/inicio');
			}else{
				HttpHandler::redirect('/catdog/sistema/signin?Error=NoAuth');
			}
		}

		public function listadoReportes(){
			$this->view->verReportes();
		}

	}

?>