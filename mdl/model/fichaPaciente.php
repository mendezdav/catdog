<?php
	class fichaPacienteModel extends object
	{

		public function cargar()
		{
			
		}//fin cargarFicha

		public function consultarMedidas()
		{
			$query = "SELECT idmedidas,nombreMedida FROM medidas WHERE idmedidas IN 
						(SELECT medidaEdad FROM fichaPaciente GROUP BY medidaEdad);";
			data_model()->executeQuery($query);		
			$ret = array();
			while ($data = data_model()->getResult()->fetch_assoc() ) {
					$ret[] = $data;
				}	
			return $ret;
		}

		public function consultarRazas()
		{
			$query = "SELECT idrazas, nombreRaza from razas";
			data_model()->executeQuery($query);
			$ret = array();
			while ($data = data_model()->getResult()->fetch_assoc() ) {
					$ret[] = $data;
				}	
			return $ret;
		}

		public function editar($id)
		{
			$query = "SELECT idfichaPaciente, nombrePeludo, edadEstimada,raza,especie,
					  fechaIngreso, Peso, motivoEntrada, motivoSalida, castrado, desparasitado, enTratamiento,procesoAdopcion FROM 
					  fichaPaciente WHERE idfichaPaciente=$id";
			return data_model()->cacheQuery($query);
		}

		public function consultarEspecieRaza()
		{			
			$query = "SELECT nombreEspecie, nombreRaza, nombrePeludo, Genero, edadEstimada, fechaIngreso, motivoEntrada, castrado, desparasitado FROM especie INNER JOIN fichapaciente ON idespecie = especie INNER JOIN razas ON idrazas=raza";
			return data_model()->cacheQuery($query);
		}
	} 
?>