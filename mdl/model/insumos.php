<?php

	class insumosModel  extends object
	{
		
		public function ObtenerRegistroExacto($id)
		{

			$qr = "select * from insumos where idinsumos=" . $id . ";";
			return data_model()->cacheQuery($qr);
			
		}

		public function obtenerMedida()
		{
			$qr = "select * from medidas;";
			return data_model()->cacheQuery($qr);
		}

		public function cambiarMedidaAjax($id){
			$query = "SELECT nombreMedida FROM insumos JOIN tipoinsumos on insumos.tipoInsumo = idtipoInsumos JOIN medidas on idmedidas=idMedida WHERE insumos.idinsumos = $id ";
			data_model()->executeQuery($query);
			$data = data_model()->getResult()->fetch_assoc();
			return $data['nombreMedida'];
		}

		public function datosInsumosAjax($id){
			$query = "SELECT * FROM insumos WHERE idinsumos = $id;";
			data_model()->executeQuery($query);
			$ret = array();
			$num = data_model()->getNumRows();
			$ret = data_model()->getResult()->fetch_assoc();
			if($num>0){
				$ret['status'] = 200;
			}else{
				$ret['status'] = 404;
			}
			return $ret;
		}

		public function consultarStockInsumo($id){
			$query = "SELECT cantidadTotal FROM insumos WHERE idinsumos = $id";
			data_model()->executeQuery($query);
			if(data_model()->getNumRows()>0){
				$data = data_model()->getResult()->fetch_assoc();
				return $data['cantidadTotal'];
			}else{
				return 0;
			}
		}

		public function medidaPorTipo($tipo)
		{
			$query  = "SELECT m.nombreMedida from 
					   tipoInsumos as ti 
					   INNER JOIN medidas as m
					   ON  ti.idMedida = m.idmedidas
					   where ti.idtipoDonacion = $tipo";
			data_model()->executeQuery($query);
			$medida = data_model()->getResults()->fetch_assoc();
			return $medida;
		}

		public function listar()
		{
			$query = "SELECT i.idinsumos, i.nombre_insumo, i.cantidadTotal, m.nombreMedida 
					FROM insumos as i
					inner join tipoinsumos as tin 
					on tin.idtipoInsumos =  i.tipoInsumo
					inner join medidas as m
					on tin.idMedida = m.idmedidas;";
			return data_model()->cacheQuery($query);
		}

		public function tipo_actual($id)
		{
			$query = "SELECT ti.tipoInsumo FROM insumos as i 
			 		 INNER JOIN tipoinsumos as ti ON 
			 		 ti.idtipoInsumos = i.tipoInsumo 
			 		 WHERE i.idinsumos = $id";
			return data_model()->cacheQuery($query);
		}
	}

?>
