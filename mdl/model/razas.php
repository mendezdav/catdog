<?php
	class razasModel extends object
	{

		public function consultar()
		{			
			$query = "SELECT COUNT(idrazas) AS cantidad, nombreRaza FROM razas INNER JOIN fichapaciente ON idrazas = raza 
			GROUP BY nombreRaza";
			data_model()->executeQuery($query);		
			return data_model()->cacheQuery($query);
		}
	} //fin raza
?>