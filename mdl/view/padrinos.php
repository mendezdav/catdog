<?php
	class padrinosView
	{
		public function ver($cache)
		{
			template()->buildFromTemplates('template.html');
			template()->addTemplateBit("contenido","historialClinico/nuevoPadrino.html");
			page()->addEstigma('resource','http://'.$_SERVER['HTTP_HOST'].'/'.WEB_DIR);			
			page()->addEstigma("listaPadrinos", array("SQL", $cache[0]));
			template()->parseOutput();
			template()->parseExtras();
			echo page()->getContent();
		}

		public function editar($cache)
		{
			template()->buildFromTemplates('template.html');
			template()->addTemplateBit('contenido',"historialClinico/editarPadrino.html");
			page()->addEstigma('resource','http://'.$_SERVER['HTTP_HOST'].'/'.WEB_DIR);	
			page()->addEstigma("listaPadrinos", array("SQL", $cache[0]));
			template()->parseOutput();
			template()->parseExtras();
			echo page()->getContent();
		}

		public function imprimirReporte($cache){
			import('common.plugins.sigma.demos.export_php.html2pdf.html2pdf');
			template()->buildFromTemplates('reportes/template.html');
			template()->addTemplateBit('contenido','reportes/padrinos.html');
			page()->addEstigma("fecha",date("y/m/d h:m:s"));
			page()->addEstigma("username",Session::getUser());
			page()->addEstigma("lista",array('SQL',$cache[0]));				
			template()->parseOutput();
			$html2pdf = new HTML2PDF('P','A4','es');
    		$html2pdf->WriteHTML(page()->getContent());
    		$html2pdf->Output('exemple.pdf');
		}
	}
?>