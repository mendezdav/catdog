<?php
	
	class razasView
	{
		public function imprimirReporte($cache){
			import('common.plugins.sigma.demos.export_php.html2pdf.html2pdf');
			template()->buildFromTemplates('reportes/template.html');
			template()->addTemplateBit('contenido','reportes/raza.html');
			page()->addEstigma("fecha",date("y/m/d h:m:s"));
			page()->addEstigma("username",Session::getUser());
			page()->addEstigma("lista",array('SQL',$cache[0]));	
			page()->addEstigma("cantidad",array('SQL',$cache[1]));			
			template()->parseOutput();
			$html2pdf = new HTML2PDF('P','A4','es');
    		$html2pdf->WriteHTML(page()->getContent());
    		$html2pdf->Output('exemple.pdf');
		}//fin function
	}

?>